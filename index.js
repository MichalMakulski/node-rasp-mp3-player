const http = require('http');
const fs = require('fs');
const URL = require('url');
const querystring = require('querystring');
const Busboy = require('busboy');
const Player = require('player');
const mp3Dir = `${__dirname}/mp3/`;

let player = undefined;

http.createServer((req, res) => {
  const url = URL.parse(req.url);
  const song = querystring.parse(url.query).song || null;
  
  if (url.pathname === '/') {
    const html = fs.createReadStream(`${__dirname}/client/index.html`);
    res.writeHead(200, {'content-type': 'text/html'});
    html.pipe(res);
    return;
  }
  
  if (url.pathname === '/app.js') {
    fs.readdir(mp3Dir, (err, files) => {
      if (err) throw err;
      fs.readFile(`${__dirname}/client/js/app.js`, 'utf8', (err, data) => {
        if (err) throw err;
        const file = data.replace('{{initialSongList}}', JSON.stringify(files));
        res.writeHead(200, {'content-type': 'application/javascript'});
        res.end(file);
      });
    });
    return;
  }
  
  if (url.pathname === '/upload-files') {
    const busboy = new Busboy({ headers: req.headers });
    
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      file.pipe(fs.createWriteStream(`${mp3Dir}${filename}`))
    });
    busboy.on('finish', function() {
      fs.readdir(mp3Dir, (err, files) => {
        if (err) throw err;
        res.writeHead(200, {'content-type': 'application/json'});
        res.end(JSON.stringify(files));
      })
    });
    req.pipe(busboy);
    return;
  }
  
  if (url.pathname === '/play') {
    
    if (player) {
      player.stop();
    } 
    
    playNext(song);
    
    return;
  }
  
  if (url.pathname === '/play-browser') {
    
    if (player) {
      player.stop();
    }
    
    const songStream = fs.createReadStream(`${mp3Dir}${song}`);
    songStream.pipe(res);
    
    return;
  }
  
  if (url.pathname === '/stop') {
    player.stop();
    console.log('Player stopped');
    res.writeHead(200, {'content-type': 'text/plain'});
    res.end('Player stopped');
  }
  
  function playNext(song) {
    player = new Player(`${__dirname}/mp3/${song}`);
    player.play((err, player) => {
      if (err) throw err;
    })
    player.on('playing',function(item){
      console.log('playing... src:', item._name);
    });
    res.writeHead(200, {'content-type': 'text/plain'});
    res.end(`Playing: ${song}`);
  }
  
}).listen(2024, () => console.log('Player started...'));
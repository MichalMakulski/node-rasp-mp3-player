;(function(){
      
  'use strict';

  const form = document.querySelector('form');
  const filesInput = form.querySelector('#filesInput');
  const statusMsg = document.querySelector('.status-msg');
  const initialSongList = {{initialSongList}};

  renderSongList(initialSongList);

  filesInput.addEventListener('change', e => {
    const submitBtn = form.querySelector('#submit-btn');
    submitBtn.disabled = !e.target.files.length;
  }, false);

  filesInput.addEventListener('change', e => {
    const submitBtn = form.querySelector('#submit-btn');

    submitBtn.disabled = !e.target.files.length;

  }, false);

  form.addEventListener('submit', e => {
    e.preventDefault();
    const data = new FormData();
    const files = form.filesInput.files;
    const opts = {
      method: 'POST',
      body: data
    }

    statusMsg.style.display = 'inline';

    for (let i = 0; i < files.length; i++) {
      data.append('song', files[i]);
    }

    fetch('/upload-files', opts)
      .then(res => res.json())
      .then(json => {
        statusMsg.style.display = 'none';
        renderSongList(json);
      });
  }, false);

  document.addEventListener('click', e => {
    const target = e.target;
    const isPlayBtn = target.classList.contains('play');
    const isPlayBrowserBtn = target.classList.contains('play-browser');
    const isStopBtn = target.classList.contains('stop');

    if (isPlayBtn) {
      const song = target.parentElement.dataset.song;
      fetch(`/play?song=${song}`)
        .then(res => res.text())
        .then(txt => console.log(txt));
    }
    if (isPlayBrowserBtn) {
      const song = target.parentElement.dataset.song;
      const msg = '<p>Just a sec...</p>';
      const playerContainer = document.querySelector('.player-container');

      playerContainer.innerHTML = msg;

      fetch(`/play-browser?song=${song}`)
        .then(res => res.blob())
        .then(blob => startPlayer(blob, song, playerContainer));
    }
    if (isStopBtn) {
      fetch('/stop')
        .then(res => res.text())
        .then(txt => console.log(txt));
    }
  }, false);

  function renderSongList (songsData) {
    const songList = document.querySelector('ul');
    const html = songsData.length ? songsData.map(song => `<li data-song="${song}">
      ${song} 
      <span class="play"></span>
      <span class="stop"></span>
      <button class="play-browser">Play on device</button>
    </li>`).join('') :  '<li>Upload some mp3s...</li>';

    songList.innerHTML = html;
  }

  function startPlayer(songData, songName, playerContainer) {
    const audio = window.URL.createObjectURL(songData);
    const player = `<audio controls src=${audio}></audio>`;
    const song = `<p>Song: ${songName}</p>`;

    playerContainer.innerHTML = player + song;
  }
})();